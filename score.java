package uml1;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

public class Score {
		public static void main(String[] args) {
			Score calculate_score = new Score();
			calculate_score.final_score();
		}
		public void final_score() {
			try {
				InputStream personInfoInputStream = Score.class.getClassLoader().getResourceAsStream("total.properties");
				Properties personInfoProperties = new Properties();
				personInfoProperties.load(personInfoInputStream);
				double max_before = Double.parseDouble(personInfoProperties.getProperty("before"));
				double max_base = Double.parseDouble(personInfoProperties.getProperty("base"));
				double max_test = Double.parseDouble(personInfoProperties.getProperty("test"));
				double max_program = Double.parseDouble(personInfoProperties.getProperty("program"));
				double max_add = Double.parseDouble(personInfoProperties.getProperty("add"));
				File file = new File("src/small.html");
				org.jsoup.nodes.Document d1 = Jsoup.parse(file, "UTF-8");
				File file2 = new File("src/all.html");
				org.jsoup.nodes.Document d2 = Jsoup.parse(file2, "UTF-8");
				double my_before = 0;
				double my_base = 0;
				double my_test = 0;
				double my_program = 0;
				double my_add = 0;
				if (d1 != null) {
					Elements elements = d1.getElementsByAttributeValue("class", "interaction-row");
					int x = 0;
					for (int i = 0; i < elements.size(); i++) {
						if (elements.get(i).child(1).child(0).toString().contains("课堂完成")
								&& elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
							Scanner sc = new Scanner(
									elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
							x = sc.nextInt();
							my_base = my_base + x;
							sc.close();
						} else if (elements.get(i).child(1).child(0).toString().contains("课堂小测")
								&& elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
							Scanner sc = new Scanner(
									elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
							x = sc.nextInt();
							my_test = my_test + x;
							sc.close();
						} else if (elements.get(i).child(1).child(0).toString().contains("编程题")
								&& elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
							Scanner sc = new Scanner(
									elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
							x = sc.nextInt();
							my_program = my_program + x;
							sc.close();
						} else if (elements.get(i).child(1).child(0).toString().contains("附加题")
								&& elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
							Scanner sc = new Scanner(
									elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
							x = sc.nextInt();
							my_add = my_add + x;
							sc.close();
						} else if (elements.get(i).child(1).child(0).toString().contains("课前自测")
								&& elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
							Scanner sc = new Scanner(
									elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
							x = sc.nextInt();
							my_before = my_before + x;
							sc.close();
						}
					}
				}
				if (d2 != null) {
					Elements element = d2.getElementsByAttributeValue("class", "interaction-row");
					int y = 0;
					for (int i = 0; i < element.size(); i++) {
						if (element.get(i).child(1).child(0).toString().contains("课前自测")
								&& element.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
							Scanner s = new Scanner(
									element.get(i).child(1).child(2).children().get(0).children().get(10).text());
							y = s.nextInt();
							my_before = my_before + y;
							s.close();
						}
					}
				}
				Score score = new Score();
				double final_score = score.Before(my_before, max_before) * 0.25 + score.Before(my_base, max_base) * 0.3
						+ score.Before(my_test, max_test) * 0.2 + score.Before(my_program, max_program) * 0.1
						+ score.Before(my_add, max_add) * 0.05;
				System.out.println("最终得分：" + String.format("%.2f", final_score));
				final_score = final_score + 6;
				System.out.println("最终得分：" + String.format("%.2f", final_score));

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		public double Before(double x, double y) {
			return x / y * 100;
		}
		public double Base(double x, double y) {
			return x / y * 100 * 0.95;
		}
		public double Test(double x, double y) {
			return x / y * 100;
		}
		public double Program(double x, double y) {
			double c = x / y * 100;
			if (c > 95) {
				c = 95;
			}
			return c;
		}
		public double Add(double x, double y) {
			double c = x / y * 100;
			if (c > 90) {
				c = 90;
			}
			return c;
		}
	}
}
